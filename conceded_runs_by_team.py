"""To read the csv file"""
import csv
import matplotlib.pyplot as plot
import helper


def extra_runs_conceded(match_file, delivery_file, year):
    '''
    Calculating the extra runs conceded by team
    '''
    teams = {}
    try:
        if not match_file.endswith(".csv") or not delivery_file.endswith(".csv") or not isinstance(year, int):
                raise TypeError
        match_id = helper.get_id_set(match_file, year)
        with open(delivery_file, 'r') as deliveryfile:
            delivery_reader = csv.DictReader(deliveryfile)
            for match in delivery_reader:
                if match['match_id'] in match_id:
                    bowling_teams = match['bowling_team']
                    extra_run = match['extra_runs']
                    if bowling_teams not in teams:
                        teams[bowling_teams] = 0
                    teams[bowling_teams] += int(extra_run)
        return teams
    except FileNotFoundError:
        return "File not found"
    except TypeError:
        return "required: csv file"
    except KeyError:
        return "wrong file"
    except OSError:
        return "File name should be string"
    except AttributeError:
        return "Something is wrong!"

extra_runs_conceded('matches.csv', 'deliveries.csv', 2008)
