import unittest
from matches_played_per_year import get_matches_per_year

class TestMatchesPerYear(unittest.TestCase):

    def test_output(self):
        expected_output = {'2008':2,'2009':2,'2010':2}
        self.assertEqual(get_matches_per_year('matches.csv'),expected_output)

    def test_filenotfound(self):
        self.assertEqual(get_matches_per_year('match.csv'),"File not found")

    def test_typeerror(self):
        self.assertEqual(get_matches_per_year('matches.txt'),"required: csv file")

    def test_keyerror(self):
        self.assertEqual(get_matches_per_year('deliveries.csv'),"wrong file")























if __name__ == '__main__':
    unittest.main()