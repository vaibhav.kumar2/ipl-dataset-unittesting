import unittest
from conceded_runs_by_team import extra_runs_conceded

class TestMatchesPerYear(unittest.TestCase):

    def test_output(self):
        expected_output = {'Royal Challengers Bangalore': 2, 'Kings XI Punjab': 0, 'Chennai Super Kings': 0}
        self.assertEqual(extra_runs_conceded('matches.csv', 'deliveries.csv', 2008), expected_output)

    def test_filenotfound(self):
        self.assertEqual(extra_runs_conceded('/matches.csv', '/deliveries.csv', 2008),"File not found")

    def test_typeerror(self):
        self.assertEqual(extra_runs_conceded('matches.txt', 'deliveries.txt', 2008), "required: csv file")

    def test_keyerror(self):
        self.assertEqual(extra_runs_conceded('deliveries.csv', 'matches.csv', 2008),"wrong file")

    def test_attributerror(self):
        self.assertEqual(extra_runs_conceded(0, 1, 2),"Something is wrong!")
