Name : IPL DATASET UNITTESTING PROJECT.
​

Description :

This project contains the possible test cases that are used to test the IPL DATASET PROJECT.
​1. File not found error
2. Type error
3. Key error
4. Os error
5. Attribute error

Modules:

1. matches_played_per_year.py
2. test_matches_played_per_year.py
3. team_won_ipl.py
4. test_ team_won_ipl.py
5. conceded_runs_by_team.py
6. test_conceded_runs_by_team.py
7. bowlers_economy.py
8. test_bowlers_economy.py
9. top_batsman.py
10. test_top_batsman.py
11. helper.py
12. test_helper.py
