import csv
def get_id_set(match_file, year):
    match_id = set()
    try:
        if not match_file.endswith(".csv") or not isinstance(year, int):
                raise TypeError
        with open(match_file, 'r') as matchfile:
            matches_reader = csv.DictReader(matchfile, delimiter=',')
            for match in matches_reader:
                season = match['season']
                if int(season) == year:
                    match_id.add(match['id'])
        return match_id
    except FileNotFoundError:
        return "File not found"
    except TypeError:
        return "required: csv file"
    except KeyError:
        return "wrong file"
    except AttributeError:
        return "Something is wrong!"
