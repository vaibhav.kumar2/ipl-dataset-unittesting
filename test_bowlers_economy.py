import unittest
from bowlers_economy import get_economy_of_bowlers

class TestMatchesPerYear(unittest.TestCase):

    def test_output(self):
        expected_output = ([('ST Jayasuriya', 14.0), ('AD Mascarenhas', 4.8), ('MS Gony', 2.4), ('MM Patel', 1.2), ('DJ Bravo', 0.0)])
        self.assertEqual(get_economy_of_bowlers("matches.csv", "deliveries.csv", 2009), expected_output)

    def test_filenotfound(self):
        self.assertEqual(get_economy_of_bowlers('/matches.csv', '/deliveries.csv', 2009),"File not found")

    def test_typeerror(self):
        self.assertEqual(get_economy_of_bowlers('matches.txt', 'deliveries.txt', 2009), "required: csv file")

    def test_keyerror(self):
        self.assertEqual(get_economy_of_bowlers('deliveries.csv', 'matches.csv', 2009),"wrong file")

    def test_attributerror(self):
        self.assertEqual(get_economy_of_bowlers(0, 1, 2),"Name is not defined!")
