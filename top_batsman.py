'''
Read the CSV file
'''
import csv
import helper

def get_top_batsman(match_file, delivery_file, year):
    '''
    Getting the top-batsman
    '''
    top_batsman = {}
    try:
        if not match_file.endswith(".csv") or not delivery_file.endswith(".csv") or not isinstance(year, int):
                raise TypeError
        match_id = helper.get_id_set(match_file, year)
        with open(delivery_file, 'r') as deliveryfile:
            deliveries_reader = csv.DictReader(deliveryfile)
            for match in deliveries_reader:
                if match['match_id'] in match_id:
                    super_over = bool(match['is_super_over'])
                    batsman_run = int(match['batsman_runs'])
                    striker = match['batsman']
                    if super_over:
                        if striker not in top_batsman:
                            top_batsman[striker] = 0
                        top_batsman[striker] += batsman_run
        return top_batsman
    except FileNotFoundError:
        return "File not found"
    except FileNotFoundError:
        return "File not found"
    except TypeError:
        return "required: csv file"
    except KeyError:
        return "wrong file"
    except OSError:
        return "File name should be string"
    except AttributeError:
        return "Name is not defined!"

get_top_batsman('matches.csv', 'deliveries.csv', 2008)
