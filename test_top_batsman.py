import unittest
from top_batsman import get_top_batsman

class TestMatchesPerYear(unittest.TestCase):

    def test_output(self):
        expected_output = {'SC Ganguly': 0, 'BB McCullum': 10, 'RT Ponting': 1, 'PA Patel': 11, 'ML Hayden': 0, 'JR Hopes': 8}
        self.assertEqual(get_top_batsman("matches.csv", "deliveries.csv", 2008), expected_output)

    def test_filenotfound(self):
        self.assertEqual(get_top_batsman('/matches.csv', '/deliveries.csv', 2008),"File not found")

    def test_typeerror(self):
        self.assertEqual(get_top_batsman('matches.txt', 'deliveries.txt', 2008), "required: csv file")

    def test_keyerror(self):
        self.assertEqual(get_top_batsman('deliveries.csv', 'matches.csv', 2008),"wrong file")

    def test_attributerror(self):
        self.assertEqual(get_top_batsman(0, 1, 2),"Name is not defined!")
