import unittest
from team_won_ipl import matches_won_by_team

class TestMatchesPerYear(unittest.TestCase):

    def test_output(self):
        expected_output = {

            'Kolkata Knight Riders': {'2008': 1, '2009': 0, '2010': 1},
            'Chennai Super Kings': {'2008': 1, '2009': 0, '2010': 0},
            'Mumbai Indians': {'2008': 0, '2009': 1, '2010': 1},
            'Royal Challengers Bangalore': {'2008': 0, '2009': 1, '2010': 0}
        }
        self.assertEqual(matches_won_by_team('matches.csv'),expected_output)

    def test_filenotfound(self):
        self.assertEqual(matches_won_by_team('match.csv'),"File not found")

    def test_typeerror(self):
        self.assertEqual(matches_won_by_team('matches.txt'),"required: csv file")

    def test_keyerror(self):
        self.assertEqual(matches_won_by_team('deliveries.csv'),"wrong file")

    def test_oserror(self):
        self.assertEqual(matches_won_by_team(1234),"File name should be string")
