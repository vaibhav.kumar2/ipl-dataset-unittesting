import unittest
from conceded_runs_by_team import extra_runs_conceded
from helper import get_id_set

class TestMatchesPerYear(unittest.TestCase):

    def test_output(self):
        expected_output = {'61', '60'}
        self.assertEqual(get_id_set('matches.csv', 2008), expected_output)

    def test_filenotfound(self):
        self.assertEqual(get_id_set('/matches.csv', 2008),"File not found")

    def test_typeerror(self):
        self.assertEqual(get_id_set('matches.txt', 2008), "required: csv file")

    def test_keyerror(self):
        self.assertEqual(get_id_set('deliveries.csv', 2008),"wrong file")

    def test_attributerror(self):
        self.assertEqual(get_id_set(1, 2),"Something is wrong!")
