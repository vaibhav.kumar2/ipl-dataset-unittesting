'''
To read the CSV file
'''
import csv
import re

def get_matches_per_year(file):
    '''
    Getting matches per year played by team
    '''
    result = {}
    try:
        if not file.endswith(".csv"):
            raise TypeError
        with open(file, 'r') as matchfile:
            matches_reader = csv.DictReader(matchfile, delimiter=',')
            for match in matches_reader:
                year = match['season']
                if year not in result:
                    result[year] = 1
                else:
                    result[year] += 1
        return result
    except KeyError:
        return "wrong file"
    except FileNotFoundError:
        return "File not found"
    except TypeError :
        return "required: csv file"



get_matches_per_year('matches.csv')
