'''
To read the CSV file
'''
import csv
import matplotlib.pyplot as plot


def find_seasons(filename):
    '''
    Getting all the seasons
    '''
    seasons = set()
    with open(filename) as matchesfile:
        matches_reader = csv.DictReader(matchesfile)
        for match in matches_reader:
            seasons.add(match['season'])
    seasons = list(seasons)
    seasons.sort()
    return seasons

def matches_won_by_team(match_file):
    '''
    Matches won by team  per year
    '''
    matches = {}
    try:
        season_set = find_seasons(match_file)
        if not match_file.endswith(".csv"):
            raise TypeError
        if type(match_file)!= str:
            return "File name should be string"
        with open(match_file, 'r') as matchfile:
            matches_reader = csv.DictReader(matchfile, delimiter=',')
            for match in matches_reader:
                match_winner_team = match['winner']
                seasons = match['season']
                if match['winner'] == "":
                    continue
                if match_winner_team not in matches:
                    matches[match_winner_team] = {}
                for current_year in season_set:
                    if current_year not in matches[match_winner_team]:
                        matches[match_winner_team][current_year] = 0
                matches[match_winner_team][seasons] += 1
        return matches
    except FileNotFoundError:
        return "File not found"
    except TypeError :
        return "required: csv file"
    except KeyError:
        return "wrong file"
    except OSError:
        return "File name should be string"



# matches_won_by_team('matches.csv')
